# coding=utf8

# produces a CSV file containing all of Dream Market's feedbacks, by scraping crawled product pages

import csv
import os
from bs4 import BeautifulSoup

saveFile = "/path/to/save/directory/dreammarket-feedbacks.csv"
crawlDir = "/path/to/dreammarket/save/directory/for/products/crawl/"

def parse(file, sf):
    contents = ""

    with open(file, 'r') as myfile:
        contents = myfile.read().replace('\n', '')

    # trim content
    contents = contents[contents.find("<div class=\"viewProduct\">"):]

    soup = BeautifulSoup(contents, 'html.parser')


    id = ""
    vendor = ""
    age = ""
    rating = ""
    description = ""

    id = file[len(crawlDir):]
    id = id[:len(id) - 5]

    tabularDetails = soup.find("div", {"class": "tabularDetails"})

    if tabularDetails != None:
        tdDivs = tabularDetails.find_all("div")

        for tdDiv in tdDivs:
            label = tdDiv.find("label")

            if label != None:
                labelText = label.get_text().strip()

                if labelText == "Vendor":
                    vendorLink = tdDiv.find("a")

                    if vendorLink != None:
                        vendor = vendorLink.get_text().strip()


    # trim content
    contents = contents[contents.find("<div class=\"ratings\">"):]

    soup = BeautifulSoup(contents, 'html.parser')


    table = soup.find("table", {"class": "hoverable"})

    if table != None:
        trs = table.find_all("tr")

        for tr in trs:
            tdAge = tr.find("td", {"class": "age"})

            if tdAge != None:
                age = tdAge.get_text().strip()

            tdRating = tr.find("td", {"class": "rating"})

            if tdRating != None:
                imgs = tdRating.find_all("img", {"src": "img/star_gold.png"})

                rating = str(len(imgs))

            tdRatingText = tr.find("td", {"class": "ratingText"})

            if tdRatingText != None:
                description = tdRatingText.get_text().strip()

            row = [id, vendor, age, rating, description]
            row = [s.encode('utf-8') for s in row]
            writer = csv.writer(sf, quoting=csv.QUOTE_ALL)
            writer.writerow(row)


with open(saveFile, 'w') as f:
    row = ["product id", "vendor", "age", "rating", "comments"]

    row = [s.encode('utf-8') for s in row]

    writer = csv.writer(f, quoting=csv.QUOTE_ALL)
    writer.writerow(row)

    for file in os.listdir(crawlDir):
        if file.endswith(".html"):
            parse(os.path.join(crawlDir, file), f)
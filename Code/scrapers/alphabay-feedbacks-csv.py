# produces a CSV file of all AlphaBay feedback, by scraping feedback data from a completed AlphaBay products crawl

import csv
import os
from bs4 import BeautifulSoup

saveFile = "/path/to/save/directory/alphabay-feedbacks.csv"
crawlDir = "/path/to/alphabay/save/directory/for/products/crawl/"

# parses a product HTML page and outputs each feedback as a row to a CSV file
def parse(file, sf):
    contents = ""

    # open and retrieve the contents from a product HTML file
    with open(file, 'r') as myfile:
        contents = myfile.read().replace('\n', '')

    id = ""
    vendor = ""
    feedback = []

    # determine the product's id from the filename
    id = file[len(crawlDir):]
    id = id[:len(id) - 5]

    # trim content to ease parsing of data
    contents = contents[contents.find("<p class=\"std\">Sold by <a class=\"std\" href=\"user.php?id="):]
    soup = BeautifulSoup(contents, 'html.parser')

    # determine the vendor's name
    for link in soup.find_all("a", {"class": "std"}):
        if "user.php?id=" in link.get("href"):
            vendor = link.get_text()
            break

    # process each feedback listed on the product page
    for slisting in soup.find_all("div", {"class": "slisting"}):
        divIndex = 0
        feedbackBuyer = ""
        feedbackDate = ""
        feedbackTime = ""
        feedbackComment = ""

        # determine the buyer, date, time and comments for each feedback
        for div in slisting.find_all("div"):
            if divIndex == 1 and div.find("a") != None:
                feedbackBuyer = div.find("a").get_text()
            elif divIndex == 2 and feedbackBuyer != "":
                feedbackDate = div.find("span").get_text()
            elif divIndex == 3 and feedbackBuyer != "":
                feedbackTime = div.find("span").get_text()
            elif divIndex == 4 and feedbackBuyer != "":
                feedbackComment = div.find("span").get_text()

            divIndex += 1

        # output a row containing the feedback data to the CSV file
        row = [id, vendor, feedbackBuyer, feedbackDate, feedbackTime, feedbackComment]
        row = [s.encode('utf-8') for s in row]
        writer = csv.writer(sf, quoting=csv.QUOTE_ALL)
        writer.writerow(row)


# create a CSV file
with open(saveFile, 'w') as f:
    # set and output the column headers to the CSV file
    row = ["product id", "vendor", "buyer", "date", "time", "comment"]
    row = [s.encode('utf-8') for s in row]
    writer = csv.writer(f, quoting=csv.QUOTE_ALL)
    writer.writerow(row)

    # parse each product HTML file
    for file in os.listdir(crawlDir):
        if file.endswith(".html"):
            parse(os.path.join(crawlDir, file), f)



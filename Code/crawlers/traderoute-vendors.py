# crawls all vendor profile pages on the TradeRoute site and saves them to a directory

import socks
import socket
import urllib
import urllib2
from bs4 import BeautifulSoup
from PIL import Image
import time
import threading
import os
import pickle
from decimal import Decimal
from io import BytesIO
import base64
import csv

# IP address of SOCKS proxy
socksIP = ""

# SOCKS proxy port
socksPort = 9050

# directory to save listing pages to
saveDir = "/path/to/traderoute/save/directory/for/vendors/crawl/"

# minimum expected size of returned HTML listing page
minSize = 6000

# CSV file containing scraped listings from previous listings crawl
listingsCsv = "/path/to/save/directory/traderoute-listings.csv"

# account credentials
credentials = ("username", "password")

# number of threads to crawl with - all threads use the same account credentials
userTotal = 1

quit = False
captchaIndex = 0
downloadErrors = []
errorLock = threading.RLock()
statusLock = threading.RLock()
status = {}
vendors = []

# ensure new outbound connections are created through the SOCKS proxy
def create_connection(address, timeout=None, source_address=None):
    sock = socks.socksocket()
    sock.connect(address)
    return sock

# handler to prevent automatic redirections of page requests
class NoRedirection(urllib2.HTTPErrorProcessor):
    def http_response(self, request, response):
        return response

    https_response = http_response

# set the value for a field used in the crawling status report
def setStatusValue(field, value):
    global statusLock
    global status

    statusLock.acquire()
    try:
        status[field] = value
    finally:
        statusLock.release()

# check if the crawl has finished or has been terminated by the user
def checkCompletion():
    global statusLock
    global credentials
    global status

    completed = False

    while (completed == False and quit == False):
        completed = True

        statusLock.acquire()
        try:
            userIndex = 0

            for i in range(0, userTotal):
                userIndex += 1

                if userIndex == 1:
                    currentRetry = status["currentRetry" + str(userIndex)]
                    totalRetry = status["totalRetry" + str(userIndex)]

                    if currentRetry != totalRetry:
                        completed = False

                currentDownload = status["current" + str(userIndex)]
                totalDownloads = status["total" + str(userIndex)]

                if currentDownload != totalDownloads:
                    completed = False

        finally:
            statusLock.release()

        time.sleep(1)

    statusLock.acquire()
    errorLock.acquire()
    try:
        print ""
        print "Finished with " + str(len(downloadErrors)) + " errors"
    finally:
        statusLock.release()
        errorLock.release()

# crawl a range of vendor profile pages with the specified username and password
def crawl(username, password, startIndex, endIndex, userIndex):
    def saveError(productID):

        global downloadErrors
        global errorLock

        errorLock.acquire()

        try:
            if (productID not in downloadErrors):
                downloadErrors.append(productID)
                with open(saveDir + "errors.txt", "wb") as fp:
                    pickle.dump(downloadErrors, fp)
        finally:
            errorLock.release()

    # return the page content of a URL, optionally posting parameters and setting requests to not automatically
    # redirect
    def sendResponse(url, data=None, noRedirect=False):
        request = urllib2.Request(url)

        if (data != None):
            request.add_data(urllib.urlencode(data))

        opener = urllib2

        if (noRedirect == False):
            opener = urllib2.build_opener(urllib2.HTTPHandler(), cookieprocessor)
        else:
            opener = urllib2.build_opener(NoRedirection, cookieprocessor)

        downloadError = False

        content = ""
        statusCode = 0

        try:
            sock = opener.open(request, timeout=60)
            content = sock.read()
            statusCode = sock.getcode()
            sock.close()
        except:
            downloadError = True

        # if response status code is not redirect or ok, an error has occurred
        if statusCode != 302 and statusCode != 200:
            downloadError = True

        # log the error if this is a request for a vendor profile page and there is an error
        if url.find("&user=") != -1 and (downloadError or (len(content) < minSize and len(content) != 0)):
            saveError(url[url.find("&user=") + 6:])

        return content

    # decode the base 64 inline CAPTCHA image and display it
    def showCaptcha(content):
        img = Image.open(BytesIO(base64.b64decode(content)))
        img.show()

    # get a list of files in the crawl directory
    files = os.listdir(saveDir)

    setStatusValue("currentRetry" + str(userIndex), "0")
    setStatusValue("totalRetry" + str(userIndex), "0")

    setStatusValue("current" + str(userIndex), "0")
    setStatusValue("total" + str(userIndex), str((endIndex - startIndex) + 1))

    startIndexOriginal = startIndex

    # determine if a crawl is being resumed and, if so, update the listing to start from
    for counter in range(endIndex, startIndex - 1, -1):
        if vendors[counter] + ".html" in files:
            startIndex = counter + 1
            break

    setStatusValue("current" + str(userIndex), str(startIndex - startIndexOriginal))

    # ensure cookies are kept and associated with the session
    cookieprocessor = urllib2.HTTPCookieProcessor()

    content = sendResponse("http://traderouteilbgzt.onion/login.php")

    soup = BeautifulSoup(content, 'html.parser')
    captcha = soup.find("div", {"class": "captcha_div"})

    loggedIn = False

    # attempt to login again if login fails
    while loggedIn == False:

        captcha_src = ""

        # get the inline base 64 for the CAPTCHA image
        if captcha != None:
            img = captcha.find("img")
            captcha_src = img.get("src")
            captcha_src = captcha_src[captcha_src.find("base64,") + 7:]

        # get the login nonce
        token = soup.find("input", {"name": "token"}).get("value")

        showCaptcha(captcha_src)

        captcha_code = raw_input("Enter Captcha code " + str(userIndex) + ": ")

        data = {}

        data["token"] = token
        data["username"] = username
        data["captcha"] = captcha_code
        data["submit"] = "Continue"

        content = sendResponse("http://traderouteilbgzt.onion/login.php", data)

        soup = BeautifulSoup(content, 'html.parser')

        # check if CAPTCHA entered was correct or if there is a problem with the password
        if soup.find("div", {"class": "captcha_div"}) == None:

            token = soup.find("input", {"name": "token"}).get("value")

            data = {}

            data["token"] = token
            data["password"] = password
            data["submitAuth"] = "Login"

            content = sendResponse("http://traderouteilbgzt.onion/login.php", data)

            soup = BeautifulSoup(content, 'html.parser')

            if "The password is not valid." not in content:
                loggedIn = True
            else:
                print "Incorrect password"
                return

        else:
            captcha = soup.find("div", {"class": "captcha_div"})
            print "Incorrect Captcha, please try again"

    global captchaIndex
    global downloadErrors
    global minSize
    global errorLock

    captchaIndex += 1

    # check if there were any errors with downloaded vendor profiles and re-attempt downloads on first thread only
    if userIndex == 1 and os.path.isfile(saveDir + "errors.txt"):

        # load list of URLs to retry from error log
        errorLock.acquire()
        try:
            with open(saveDir + "errors.txt", "rb") as fp:
                retries = pickle.load(fp)
        finally:
            errorLock.release()

        downloadErrors = retries[:]

        setStatusValue("totalRetry" + str(userIndex), str(len(retries)))

        retryIndex = 0

        # retry each previously erroneous URL and save it on success
        for retry in retries:
            retryIndex += 1

            setStatusValue("currentRetry" + str(userIndex), str(retryIndex))

            content = sendResponse("http://traderouteilbgzt.onion/?page=profile&user=" + str(retry), None, True)

            if len(content) > minSize:
                text_file = open(saveDir + str(retry) + ".html", "w")
                text_file.write(content)
                text_file.close()

            if len(content) > minSize or len(content) == 0:
                errorLock.acquire()

                try:
                    downloadErrors.remove(retry)
                    with open(saveDir + "errors.txt", "wb") as fp:
                        pickle.dump(downloadErrors, fp)
                finally:
                    errorLock.release()

            # delay next crawl by 1 second to ensure DDOS defences are not activated
            time.sleep(1)

            if quit:
                break

    # crawl each vendor profile HTML page
    for i in range(startIndex, endIndex + 1):
        setStatusValue("current" + str(userIndex), str(i - startIndexOriginal))


        content = sendResponse("http://traderouteilbgzt.onion/?page=profile&user=" + vendors[i], None, True)

        if len(content) != 0:
            text_file = open(saveDir + vendors[i] + ".html", "w")
            text_file.write(content)
            text_file.close()

        # delay next crawl by 1 second to ensure DDOS defences are not activated
        time.sleep(1)

        if quit:
            break

        setStatusValue("current" + str(userIndex), str((endIndex - startIndexOriginal) + 1))

# SOCKS proxy configuration setup
socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, socksIP, socksPort)

# patch the socket module - to ensure DNS lookups are done over TOR
# https://stackoverflow.com/questions/5148589/python-urllib-over-tor
socket.socket = socks.socksocket
socket.create_connection = create_connection

userIndex = 0

lastIndex = 0

vendorList = []

# load in the scraped listings from the previously completed listings crawl
with open(listingsCsv, 'rb') as f:
    reader = csv.reader(f)
    vendorList = list(reader)

# remove header from list
if len(vendorList) > 0:
    vendorList = vendorList[1:]

# ensure there are no duplicate vendors
for row in vendorList:
    vendor = row[63]
    if vendor not in vendors and len(vendor) > 1:
        vendors.append(row[63])

lastIndex = len(vendors) - 1

# split the crawling load between the specified number of threads
for i in range(0, userTotal):
    userIndex += 1
    startIndex = (((lastIndex / userTotal) * userIndex) - (lastIndex / userTotal))
    if userIndex != 1:
        startIndex += 1
    endIndex = ((lastIndex / userTotal) * userIndex)

    if (userIndex == userTotal):
        endIndex = lastIndex

    username, password = credentials

    crawl_thread = threading.Thread(target=crawl, args=(username, password, startIndex, endIndex, userIndex))

    crawl_thread.start()

    # wait until this thread has spawned, before spawning the next one
    while (captchaIndex != userIndex):
        time.sleep(1)

check_completion_thread = threading.Thread(target=checkCompletion, args=())
check_completion_thread.start()

# display menu options to user
while (quit == False):
    commandInput = raw_input("Enter (s)tatus or (q)uit: ")

    if commandInput == "q":
        quit = True
    elif commandInput == "s":
        statusLock.acquire()
        try:
            # output the status report
            userIndex = 0

            for i in range(0, userTotal):
                userIndex += 1

                noThread = str(userIndex)
                currentDownload = status["current" + str(userIndex)]
                totalDownloads = status["total" + str(userIndex)]
                progress = str(round(Decimal((float(currentDownload) / float(totalDownloads)) * 100), 2))
                currentRetry = status["currentRetry" + str(userIndex)]
                totalRetry = status["totalRetry" + str(userIndex)]

                if currentRetry != totalRetry:
                    progress = str(round(Decimal(((float(currentDownload) + float(currentRetry)) / (
                        float(totalDownloads) + float(totalRetry))) * 100), 2))
                    print "Thread " + noThread + ": " + progress + "% complete (" + currentRetry + "/" + totalRetry + " retries & " + currentDownload + "/" + totalDownloads + ")"
                else:
                    progress = str(round(Decimal((float(currentDownload) / float(totalDownloads)) * 100), 2))
                    print "Thread " + noThread + ": " + progress + "% complete (" + currentDownload + "/" + totalDownloads + ")"

        finally:
            statusLock.release()

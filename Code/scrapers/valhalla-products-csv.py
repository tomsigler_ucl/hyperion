# coding=utf8

# produces a CSV file of all Valhalla products, by scraping products data from a completed products crawl

import csv
import os
from bs4 import BeautifulSoup
import time
import codecs

saveFile = "/path/to/save/directory/valhalla-products.csv"
crawlDir = "/path/to/valhalla/save/directory/for/products/crawl/"


def parse(file, sf):
    contents = ""

    with codecs.open(file, 'r', encoding='utf8') as myfile:
        contents = myfile.read().replace('\n', '')

    id = ""
    epoch = ""
    dtImage = ""
    name = ""
    priceUsd = ""
    priceBtc = ""
    stock = ""
    auction = ""
    vendorProfile = ""
    vendor = ""
    feedbackPositive = ""
    feedbackNegative = ""
    originCountry = ""
    shipsTo = ""
    shippingOptions = []
    description = ""
    shippingFee = ""
    categories = []


    id = file[len(crawlDir):]
    id = id[:len(id) - 5]

    soup = BeautifulSoup(contents, 'html.parser')

    imgThumbnail = soup.find("img", {"class": "img-thumbnail"})
    if imgThumbnail != None:
        src = imgThumbnail.get("src")
        if "?" in src:
            epoch = src[src.find("?") + 1:]
            dtImage = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(float(epoch)))

    h1 = soup.find("h1")
    if h1 != None:
        name = h1.get_text()

    # trim content
    contents = contents[contents.find("</h1>") + 5:]
    soup = BeautifulSoup(contents, 'html.parser')

    liIndex = 0

    for li in soup.find_all("li"):
        if liIndex == 0:
            prices = li.get_text()

            if "USD" in prices:
                priceUsd = prices[0:prices.find(" USD")]
            if "BTC" in prices:
                priceBtc = prices[prices.find("(") + 1:prices.find(" BTC")]
        elif liIndex == 1:
            stock = li.get_text()
        elif liIndex == 2:
            auction = li.get_text()
        elif liIndex == 3:
            liA = li.find("a")
            if liA != None:
                vendorProfile = liA.get("href")
                vendor = liA.get_text()

            for liSpan in soup.find_all("span"):
                if liSpan.get("class") != None:
                    if "green" in liSpan.get("class"):
                        feedbackPositive = liSpan.get_text()
                    elif "red" in liSpan.get("class"):
                        feedbackNegative = liSpan.get_text()

        elif liIndex == 4:
            deliveryInfo = li.get_text().encode('utf-8')
            if deliveryInfo.find("→"):
                originCountry = deliveryInfo[:deliveryInfo.find("→") - 1]
                shipsTo = deliveryInfo[deliveryInfo.find("→") + 4:]
            else:
                originCountry = deliveryInfo

        liIndex += 1

    for option in soup.find_all("option"):
        shippingOptions.append(option.get_text())

    divDescription = soup.find("div", {"class": "description"})
    if divDescription != None:
        description = divDescription.get_text()

    pShippingFeeInfo = soup.find("p", {"class": "shipping_fee_info"})
    if pShippingFeeInfo != None:
        shippingFee = pShippingFeeInfo.get_text()

    divBreadcrumb = soup.find("div", {"id": "bottom_breadcrumb"})
    if divBreadcrumb != None:
        liIndex = 0
        for li in divBreadcrumb.find_all("li"):
            if liIndex < 3:
                categories.append(li.get_text())
            liIndex += 1


    category1 = ""
    if len(categories) > 0:
        category1 = categories[0]

    category2 = ""
    if len(categories) > 1:
        category2 = categories[1]

    category3 = ""
    if len(categories) > 2:
        category3 = categories[2]


    row = [id, name, category1, category2, category3, dtImage, priceUsd, priceBtc, stock, auction,
           vendorProfile, vendor, feedbackPositive, feedbackNegative, originCountry, shipsTo, shippingFee]
    row = [s.encode('utf-8') for s in row]
    writer = csv.writer(sf, quoting=csv.QUOTE_ALL)
    writer.writerow(row)


with open(saveFile, 'w') as f:
    row = ["id", "name", "category1", "category2", "category3", "dtImage", "priceUsd", "priceBtc", "stock",
           "auction", "vendorProfile", "vendor", "feedbackPositive", "feedbackNegative", "originCountry", "shipsTo", "shippingFee"]

    row = [s.encode('utf-8') for s in row]

    writer = csv.writer(f, quoting=csv.QUOTE_ALL)
    writer.writerow(row)

    for file in os.listdir(crawlDir):
        if file.endswith(".html"):
            parse(os.path.join(crawlDir, file), f)
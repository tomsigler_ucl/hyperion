# README #

Evaluating Operation Hyperion - UCL MSc Information Security Project 2017

This repository contains:

- Crawlers for AlphaBay, Valhalla, TradeRoute and Dream Market DNMs
- Scrapers to produce datasets
- Code to analyse datasets
- Datasets including crawled HTML pages, produced CSV files, results of analysis

Python 2.7.10 and Tor configured as a SOCKS proxy is required to run code listings

The following external libraries also need to be installed with pip:
- PySocks
- Beautiful Soup
- PIL
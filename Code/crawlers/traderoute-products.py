# crawls all product pages on the TradeRoute site and saves them to a directory

import socks
import socket
import urllib
import urllib2
from bs4 import BeautifulSoup
from PIL import Image
import time
import threading
import os
import pickle
from decimal import Decimal
from io import BytesIO
import base64
import csv

# IP address of SOCKS proxy
socksIP = ""

# SOCKS proxy port
socksPort = 9050

# directory to save listing pages to
saveDir = "/path/to/traderoute/save/directory/for/products/crawl/"

# minimum expected size of returned HTML listing page
minSize = 8000

# CSV file containing scraped listings from previous listings crawl
listingsCsv = "/path/to/save/directory/traderoute-listings.csv"

# account credentials
credentials = ("username", "password")

# number of threads to crawl with - all threads use the same account credentials
userTotal = 1

quit = False
captchaIndex = 0
downloadErrors = []
errorLock = threading.RLock()
statusLock = threading.RLock()
status = {}
products = []

# ensure new outbound connections are created through the SOCKS proxy
def create_connection(address, timeout=None, source_address=None):
    sock = socks.socksocket()
    sock.connect(address)
    return sock

# handler to prevent automatic redirections of page requests
class NoRedirection(urllib2.HTTPErrorProcessor):
    def http_response(self, request, response):
        return response

    https_response = http_response

# set the value for a field used in the crawling status report
def setStatusValue(field, value):
    global statusLock
    global status

    statusLock.acquire()
    try:
        status[field] = value
    finally:
        statusLock.release()

# check if the crawl has finished or has been terminated by the user
def checkCompletion():
    global statusLock
    global credentials
    global status

    completed = False

    while (completed == False and quit == False):
        completed = True

        statusLock.acquire()
        try:
            userIndex = 0

            for i in range(0, userTotal):
                userIndex += 1

                if userIndex == 1:
                    currentRetry = status["currentRetry" + str(userIndex)]
                    totalRetry = status["totalRetry" + str(userIndex)]

                    if currentRetry != totalRetry:
                        completed = False

                currentDownload = status["current" + str(userIndex)]
                totalDownloads = status["total" + str(userIndex)]

                if currentDownload != totalDownloads:
                    completed = False

        finally:
            statusLock.release()

        time.sleep(1)

    statusLock.acquire()
    errorLock.acquire()
    try:
        print ""
        print "Finished with " + str(len(downloadErrors)) + " errors"
    finally:
        statusLock.release()
        errorLock.release()


# crawl a range of product pages with the specified username and password
def crawl(username, password, startIndex, endIndex, userIndex):
    def saveError(productID):

        global downloadErrors
        global errorLock

        errorLock.acquire()

        try:
            if (productID not in downloadErrors):
                downloadErrors.append(productID)
                with open(saveDir + "errors.txt", "wb") as fp:
                    pickle.dump(downloadErrors, fp)
        finally:
            errorLock.release()

    # return the page content of a URL, optionally posting parameters and setting requests to not automatically
    # redirect
    def sendResponse(url, data=None, noRedirect=False):
        request = urllib2.Request(url)

        if (data != None):
            request.add_data(urllib.urlencode(data))

        opener = urllib2

        if (noRedirect == False):
            opener = urllib2.build_opener(urllib2.HTTPHandler(), cookieprocessor)
        else:
            opener = urllib2.build_opener(NoRedirection, cookieprocessor)

        downloadError = False

        content = ""
        statusCode = 0

        try:
            sock = opener.open(request, timeout=60)
            content = sock.read()
            statusCode = sock.getcode()
            sock.close()
        except:
            downloadError = True

        # if response status code is not redirect or ok, an error has occurred
        if statusCode != 302 and statusCode != 200:
            downloadError = True

        # log the error if this is a request for a products page and there is an error
        if url.find("&lid=") != -1 and (downloadError or (len(content) < minSize and len(content) != 0)):
            saveError(url[url.find("&lid=") + 5:])

        return content

    # perform a HEAD request with the Referer header set to a specified value
    # returns the date and time that an image was last modified
    def lastModified(url, referer):
        request = urllib2.Request(url)

        opener = urllib2.build_opener(urllib2.HTTPHandler(), cookieprocessor)


        lastModified = ""

        try:
            request.get_method = lambda: 'HEAD'
            request.add_header("Referer", referer)

            response = opener.open(request, timeout=60)
            response.close()

            lastModified = response.info().getheader("Last-Modified")
        except:
            pass

        return lastModified

    # decode the base 64 inline CAPTCHA image and display it
    def showCaptcha(content):
        img = Image.open(BytesIO(base64.b64decode(content)))
        img.show()

    # get a list of files in the crawl directory
    files = os.listdir(saveDir)

    setStatusValue("currentRetry" + str(userIndex), "0")
    setStatusValue("totalRetry" + str(userIndex), "0")

    setStatusValue("current" + str(userIndex), "0")
    setStatusValue("total" + str(userIndex), str((endIndex - startIndex) + 1))

    startIndexOriginal = startIndex

    # determine if a crawl is being resumed and, if so, update the listing to start from
    for counter in range(endIndex, startIndex - 1, -1):
        if products[counter] + "-1.html" in files:
            startIndex = counter + 1
            break

    setStatusValue("current" + str(userIndex), str(startIndex - startIndexOriginal))

    # ensure cookies are kept and associated with the session
    cookieprocessor = urllib2.HTTPCookieProcessor()

    content = sendResponse("http://traderouteilbgzt.onion/login.php")

    soup = BeautifulSoup(content, 'html.parser')
    captcha = soup.find("div", {"class": "captcha_div"})

    loggedIn = False

    # attempt to login again if login fails
    while loggedIn == False:

        captcha_src = ""

        # get the inline base 64 for the CAPTCHA image
        if captcha != None:
            img = captcha.find("img")
            captcha_src = img.get("src")
            captcha_src = captcha_src[captcha_src.find("base64,") + 7:]

        # get the login nonce
        token = soup.find("input", {"name": "token"}).get("value")

        showCaptcha(captcha_src)

        captcha_code = raw_input("Enter Captcha code " + str(userIndex) + ": ")

        data = {}

        data["token"] = token
        data["username"] = username
        data["captcha"] = captcha_code
        data["submit"] = "Continue"

        content = sendResponse("http://traderouteilbgzt.onion/login.php", data)

        soup = BeautifulSoup(content, 'html.parser')

        # check if CAPTCHA entered was correct or if there is a problem with the password
        if soup.find("div", {"class": "captcha_div"}) == None:

            token = soup.find("input", {"name": "token"}).get("value")

            data = {}

            data["token"] = token
            data["password"] = password
            data["submitAuth"] = "Login"

            content = sendResponse("http://traderouteilbgzt.onion/login.php", data)

            soup = BeautifulSoup(content, 'html.parser')

            if "The password is not valid." not in content:
                loggedIn = True
            else:
                print "Incorrect password"
                return

        else:
            captcha = soup.find("div", {"class": "captcha_div"})
            print "Incorrect Captcha, please try again"

    global captchaIndex
    global downloadErrors
    global minSize
    global errorLock

    captchaIndex += 1

    # check if there were any errors with downloaded products and re-attempt downloads on first thread only
    if userIndex == 1 and os.path.isfile(saveDir + "errors.txt"):

        # load list of URLs to retry from error log
        errorLock.acquire()
        try:
            with open(saveDir + "errors.txt", "rb") as fp:
                retries = pickle.load(fp)
        finally:
            errorLock.release()

        downloadErrors = retries[:]

        setStatusValue("totalRetry" + str(userIndex), str(len(retries)))

        retryIndex = 0

        # retry each previously erroneous URL and save it on success
        for retry in retries:
            retryIndex += 1

            setStatusValue("currentRetry" + str(userIndex), str(retryIndex))

            fbPages = 1
            fbIndex = 1

            # save each page of feedback for a product
            while fbIndex <= fbPages:
                content = sendResponse("http://traderouteilbgzt.onion/?page=listing&pg=" + str(fbIndex) + "&lid=" + str(retry), None, True)

                if len(content) > minSize:

                    if fbIndex == 1:
                        soup = BeautifulSoup(content, 'html.parser')
                        divImage = soup.find("div", {"class": "listing_image"})
                        if divImage != None:
                            # add the date that the product image was last modified to the HTML
                            img = divImage.find("img")
                            if img != None:
                                imgSrc = img.get("src")
                                lm = lastModified("http://traderouteilbgzt.onion/" + imgSrc,
                                                  "http://traderouteilbgzt.onion/?page=listing&pg=" + str(
                                                      fbIndex) + "&lid=" + str(retry))

                                if lm != "" and "\" alt=\"Product image\">" in content:
                                    content = content[:content.find("\" alt=\"Product image\">")] + "?" + lm + content[content.find("\" alt=\"Product image\">"):]

                    text_file = open(saveDir + str(retry) + "-" + str(fbIndex) + ".html", "w")
                    text_file.write(content)
                    text_file.close()

                    # determine number of feedback pages for product
                    if fbIndex == 1:
                        soup = BeautifulSoup(content, 'html.parser')
                        links = soup.find_all("a", {"class": "pagination_link"})

                        for link in links:
                            if link.get("title") == "Last page":
                                href = link.get("href")
                                if "&pg=" in href:
                                    fbPages = int(href[href.find("&pg=") + 4:])

                if len(content) > minSize or len(content) == 0:
                    errorLock.acquire()

                    try:
                        downloadErrors.remove(retry)
                        with open(saveDir + "errors.txt", "wb") as fp:
                            pickle.dump(downloadErrors, fp)
                    finally:
                        errorLock.release()

                # delay next crawl by 1 second to ensure DDOS defences are not activated
                time.sleep(1)

                if quit:
                    break

                fbIndex += 1

    # crawl each product HTML page and all pages of feedback for the product
    for i in range(startIndex, endIndex + 1):
        setStatusValue("current" + str(userIndex), str(i - startIndexOriginal))

        fbPages = 1
        fbIndex = 1

        while fbIndex <= fbPages:
            content = sendResponse(
                "http://traderouteilbgzt.onion/?page=listing&pg=" + str(fbIndex) + "&lid=" + products[i], None, True)

            if len(content) != 0:

                if fbIndex == 1:
                    soup = BeautifulSoup(content, 'html.parser')
                    divImage = soup.find("div", {"class": "listing_image"})
                    if divImage != None:
                        img = divImage.find("img")
                        if img != None:
                            imgSrc = img.get("src")
                            lm = lastModified("http://traderouteilbgzt.onion/" + imgSrc, "http://traderouteilbgzt.onion/?page=listing&pg=" + str(fbIndex) + "&lid=" + products[i])

                            if lm != "" and "\" alt=\"Product image\">" in content:
                                content = content[:content.find("\" alt=\"Product image\">")] + "?" + lm + content[content.find("\" alt=\"Product image\">"):]

                text_file = open(saveDir + products[i] + "-" + str(fbIndex) + ".html", "w")
                text_file.write(content)
                text_file.close()

                if fbIndex == 1:
                    soup = BeautifulSoup(content, 'html.parser')
                    links = soup.find_all("a", {"class": "pagination_link"})

                    for link in links:
                        if link.get("title") == "Last page":
                            href = link.get("href")
                            if "&pg=" in href:
                                fbPages = int(href[href.find("&pg=") + 4:])

            # delay next crawl by 1 second to ensure DDOS defences are not activated
            time.sleep(1)

            if quit:
                break

            fbIndex += 1

        setStatusValue("current" + str(userIndex), str((endIndex - startIndexOriginal) + 1))

# SOCKS proxy configuration setup
socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, socksIP, socksPort)

# patch the socket module - to ensure DNS lookups are done over TOR
# https://stackoverflow.com/questions/5148589/python-urllib-over-tor
socket.socket = socks.socksocket
socket.create_connection = create_connection

userIndex = 0

lastIndex = 0

productList = []

# load in the scraped listings from the previously completed listings crawl
with open(listingsCsv, 'rb') as f:
    reader = csv.reader(f)
    productList = list(reader)

# remove header from list
if len(productList) > 0:
    productList = productList[1:]

# ensure there are no duplicate products
for row in productList:
    product = row[0]
    if product not in products and len(product) > 1:
        products.append(row[0])

lastIndex = len(products) - 1

# split the crawling load between the specified number of threads
for i in range(0, userTotal):
    userIndex += 1
    startIndex = (((lastIndex / userTotal) * userIndex) - (lastIndex / userTotal))
    if userIndex != 1:
        startIndex += 1
    endIndex = ((lastIndex / userTotal) * userIndex)

    if (userIndex == userTotal):
        endIndex = lastIndex

    username, password = credentials

    crawl_thread = threading.Thread(target=crawl, args=(username, password, startIndex, endIndex, userIndex))

    crawl_thread.start()

    # wait until this thread has spawned, before spawning the next one
    while (captchaIndex != userIndex):
        time.sleep(1)

check_completion_thread = threading.Thread(target=checkCompletion, args=())
check_completion_thread.start()

# display menu options to user
while (quit == False):
    commandInput = raw_input("Enter (s)tatus or (q)uit: ")

    if commandInput == "q":
        quit = True
    elif commandInput == "s":
        statusLock.acquire()
        try:
            # output the status report
            userIndex = 0

            for i in range(0, userTotal):
                userIndex += 1

                noThread = str(userIndex)
                currentDownload = status["current" + str(userIndex)]
                totalDownloads = status["total" + str(userIndex)]
                progress = str(round(Decimal((float(currentDownload) / float(totalDownloads)) * 100), 2))
                currentRetry = status["currentRetry" + str(userIndex)]
                totalRetry = status["totalRetry" + str(userIndex)]

                if currentRetry != totalRetry:
                    progress = str(round(Decimal(((float(currentDownload) + float(currentRetry)) / (
                        float(totalDownloads) + float(totalRetry))) * 100), 2))
                    print "Thread " + noThread + ": " + progress + "% complete (" + currentRetry + "/" + totalRetry + " retries & " + currentDownload + "/" + totalDownloads + ")"
                else:
                    progress = str(round(Decimal((float(currentDownload) / float(totalDownloads)) * 100), 2))
                    print "Thread " + noThread + ": " + progress + "% complete (" + currentDownload + "/" + totalDownloads + ")"

        finally:
            statusLock.release()

# produces a CSV file of all AlphaBay products, by scraping product data from a completed AlphaBay products crawl

import csv
import os
from bs4 import BeautifulSoup

saveFile = "/path/to/save/directory/alphabay-products.csv"
crawlDir = "/path/to/alphabay/save/directory/for/products/crawl/"

# parses a product HTML page and outputs product as a row to a CSV file
def parse(file, sf):
    contents = ""

    # open and retrieve the contents from a product HTML file
    with open(file, 'r') as myfile:
        contents = myfile.read().replace('\n', '')

    id = ""
    name = ""
    categories = []
    vendor = ""
    noSold = ""
    dateLive = ""
    autoDispatch = ""
    levelVendor = ""
    levelTrust = ""
    productClass = ""
    quantityLeft = ""
    endsIn = ""
    originCountry = ""
    shipsTo = ""
    payment = ""
    postage = []
    purchasePrice = ""
    purchaseOther = ""
    description = ""

    # determine the product's id from the filename
    id = file[len(crawlDir):]
    id = id[:len(id)-5]

    soup = BeautifulSoup(contents, 'html.parser')

    # determine product name
    for h1 in soup.find_all("h1", { "class" : "std" }):
        name = h1.get_text()

    # determine product category and sub-categories
    for navbar in soup.find_all("a", { "class" : "navbar" }):
        href = navbar.get("href")

        if "search.php" in href:
            categories.append(navbar.get_text())
        elif href == "#":
            break

    # trim content to ease parsing of data
    contents = contents[contents.find("<p class=\"std\">Sold by <a class=\"std\" href=\"user.php?id="):]
    soup = BeautifulSoup(contents, 'html.parser')

    # determine vendor
    for link in soup.find_all("a", { "class" : "std" }):
        if "user.php?id=" in link.get("href"):
            vendor = link.get_text()
            break

    # determine number sold, date product was listed, and auto-dispatch
    iIndex = 0
    for i in soup.find_all("i"):
        if iIndex == 0:
            noSold = i.get_text()
        elif iIndex == 1:
            dateLive = i.get_text()
        elif iIndex == 2:
            autoDispatch = i.get_text()

        iIndex += 1

    # determine vendor's level and trust level
    for span in soup.find_all("span", { "class" : "level" }):
        if levelVendor == "":
            levelVendor = span.get_text()[13:]
        else:
            levelTrust = span.get_text()[12:]

    # determine stock, delivery and payment information
    divIndex = 0
    for div in soup.find_all("div"):
        if div.get("style") != None and (div.get("style") == "margin-left:120px;" or div.get("style") == "margin-left:120px;;" or div.get("style") == "margin-left:120px;white-space:normal;"):
            
            spanText = div.find("span").get_text()
            
            if divIndex == 0:
                productClass = spanText
            elif divIndex == 1:
                quantityLeft = spanText
            elif divIndex == 2:
                endsIn = spanText
            elif divIndex == 3:
                originCountry = spanText
            elif divIndex == 4:
                shipsTo = spanText
            elif divIndex == 5:
                payment = spanText

            divIndex += 1

    # determine delivery options
    if soup.find("select", { "name" : "postageid" }) != None:
        for option in soup.find("select", { "name" : "postageid" }).find_all("option"):
            postage.append(option.get_text())

    # determine price
    for div in soup.find_all("div", { "class" : "tcl" }):
        if div.get("style") != None and div.get("style") == "width:50%;display:block;":
            purchasePrice = div.find("span").get_text()
            purchasePrice = purchasePrice[purchasePrice.find(":") + 2:]

    # determine other pricing information
    for span in soup.find_all("span", { "class" : "grey" }):
        purchaseOther = span.get_text()

    # determine description
    divContent = soup.find("div", { "id" : "div_content1" })
    if divContent != None:
        for p in divContent.find("p"):
            description = p

    # split category and sub-categories into separate fields
    category1 = ""
    if len(categories) > 0:
        category1 = categories[0]
    
    category2 = ""
    if len(categories) > 1:
        category2 = categories[1]
    
    category3 = ""
    if len(categories) > 2:
        category3 = categories[2]


    # output row of product data to CSV file
    row = [id, name, category1, category2, category3, vendor, noSold, dateLive, autoDispatch, levelVendor, levelTrust,
           productClass, quantityLeft, endsIn, originCountry, shipsTo, payment, purchasePrice, purchaseOther]
    row = [s.encode('utf-8') for s in row]
    writer = csv.writer(sf, quoting=csv.QUOTE_ALL)
    writer.writerow(row)


# create a CSV file
with open(saveFile, 'w') as f:
    row = ["id", "name", "category1", "category2", "category3", "vendor", "noSold", "dateLive", "autoDispatch",
           "levelVendor", "levelTrust", "productClass", "quantityLeft", "endsIn", "originCountry", "shipsTo",
           "payment", "purchasePrice", "purchaseOther"]
    row = [s.encode('utf-8') for s in row]
    writer = csv.writer(f, quoting=csv.QUOTE_ALL)
    writer.writerow(row)

    # parse each product HTML file
    for file in os.listdir(crawlDir):
        if file.endswith(".html"):
            parse(os.path.join(crawlDir, file), f)



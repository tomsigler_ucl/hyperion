# produce a CSV list containing unique vendors, determined by linking public PGP keys together
# requires a scrape of each site's vendors to be done first, and for AlphaBay's PGP keys to have
# been retrieved from Grams first

import csv

saveFile = "/path/to/save/directory/vendors-unique.csv"

listingFiles = ["/path/to/save/directory/vendors-alphabay.csv",
                "/path/to/save/directory/valhalla-vendors.csv",
                "/path/to/save/directory/traderoute-vendors.csv",
                "/path/to/save/directory/dreammarket-vendors.csv"]
dnmAbbreviations = ["A", "V", "T", "D"]
nameIndices = [0, 1, 0, 0]
keyIndices = [1, 9, 2, 2]

noFiles = len(listingFiles)

noKeys = []

vList = {}

for i in range(0, noFiles):
    listingFile = listingFiles[i]

    nameIndex = nameIndices[i]
    keyIndex = keyIndices[i]
    dnmAbbreviation = dnmAbbreviations[i]

    vDNM = []

    # load list of vendors for a site
    with open(listingFile, 'rb') as f:
        reader = csv.reader(f)
        vDNM = list(reader)

    # remove header from list
    if len(vDNM) > 0:
        vDNM = vDNM[1:]

    for row in vDNM:
        name = row[nameIndex].strip() + " " + dnmAbbreviation
        key = row[keyIndex].strip()

        # trim key, so that it only contains base 64 portion of key, to enable more accurate matching
        if key == "" or "-----BEGIN PGP PUBLIC KEY BLOCK-----" not in key or "-----END" not in key:
            noKeys.append(name)
        else:
            key = key[key.find("-----BEGIN PGP PUBLIC KEY BLOCK-----") + 36:]
            key = key[:key.find("-----END")]

            while "Comment: " in key:
                key = key[key.find("Comment: ") + 9:]

                if key.find("\n") != -1 and ((key.find("\r") != -1 and key.find("\n") < key.find("\r")) or key.find("\r") == -1):
                    key = key[key.find("\n"):]
                elif key.find("\r") != -1:
                    key = key[key.find("\r"):]

            while "Version: " in key:
                key = key[key.find("Version: ") + 9:]
                if key.find("\n") != -1 and ((key.find("\r") != -1 and key.find("\n") < key.find("\r")) or key.find("\r") == -1):
                    key = key[key.find("\n"):]
                elif key.find("\r") != -1:
                    key = key[key.find("\r"):]

            while "Charset: " in key:
                key = key[key.find("Charset: ") + 9:]
                if key.find("\n") != -1 and ((key.find("\r") != -1 and key.find("\n") < key.find("\r")) or key.find("\r") == -1):
                    key = key[key.find("\n"):]
                elif key.find("\r") != -1:
                    key = key[key.find("\r"):]

            if ")" in key:
                key = key[key.find(")") + 1:]

            if "GnuPG v2" in key:
                key = key[key.find("GnuPG v2") + 8:]

            key = key.replace("\n", "").replace("\r", "")
            key = key.strip()

            # if the PGP key is not in our list, add it is the dictionary key with a value of the vendor's name, otherwise
            # add the vendor's name as another account to the existing PGP key
            if key not in vList:
                vList[key] = [name]
            else:
                names = vList[key]
                names.append(name)
                vList[key] = names

# output a list of all vendors linked by PGP key, with up to 11 vendor accounts per key
with open(saveFile, 'wb') as f:
    row = ["PGP key", "No. accounts", "Account 1", "Account 2", "Account 3", "Account 4", "Account 5", "Account 6", "Account 7", "Account 8", "Account 9", "Account 10", "Account 11"]

    writer = csv.writer(f, quoting=csv.QUOTE_ALL)
    writer.writerow(row)

    for key in sorted(vList.iterkeys()):
        row = [key, str(len(vList[key]))]
        row.extend(vList[key])

        writer = csv.writer(f, quoting=csv.QUOTE_ALL)
        writer.writerow(row)

    for vendor in noKeys:
        row = ["", vendor]

        writer = csv.writer(f, quoting=csv.QUOTE_ALL)
        writer.writerow(row)

# produces a summary of sales (feedbacks) broken down by day for TradeRoute and AlphaBay
# requires feedbacks from both sites to have been scraped

import csv
from dateutil.parser import parse

saveFileTradeRoute = "/path/to/save/directory/sales-traderoute.csv"
saveFileAlphaBay = "/path/to/save/directory/sales-alphabay.csv"
fbAlphaBayFile = "/path/to/save/directory/alphabay-feedbacks.csv"
fbTradeRouteFile = "/path/to/save/directory/traderoute-feedbacks.csv"


fbList = {}



fbTradeRoute = []

# load in all trade route feedbacks
with open(fbTradeRouteFile, 'rb') as f:
    reader = csv.reader(f)
    fbTradeRoute = list(reader)

# remove header from list
if len(fbTradeRoute) > 0:
    fbTradeRoute = fbTradeRoute[1:]

# produce summary of date and number of sales (feedbacks) for that date
for row in fbTradeRoute:
    fbDate = row[2]

    parsedDate = parse(fbDate)

    key = parsedDate.strftime("%Y-%m-%d")

    if parsedDate.strftime("%Y-%m-%d") >= "2016-01-01" and parsedDate.strftime("%Y-%m-%d") <= "2017-03-31":
        if not key in fbList:
            fbList[key] = 1
        else:
            fbList[key] += 1

# output summary to CSV file
with open(saveFileTradeRoute, 'w') as f:
    row = ["date", "sales"]

    row = [s.encode('utf-8') for s in row]

    writer = csv.writer(f, quoting=csv.QUOTE_ALL)
    writer.writerow(row)

    for key in sorted(fbList.iterkeys()):
        row = [key, str(fbList[key])]
        row = [s.encode('utf-8') for s in row]
        writer = csv.writer(f, quoting=csv.QUOTE_ALL)
        writer.writerow(row)



fbList = {}

fbAlpha = []

# load in all AlphaBay feedbacks
with open(fbAlphaBayFile, 'rb') as f:
    reader = csv.reader(f)
    fbAlpha = list(reader)

# remove header from list
if len(fbAlpha) > 0:
    fbAlpha = fbAlpha[1:]

# produce summary of date and number of sales (feedbacks) for that date
for row in fbAlpha:
    fbDate = row[3]

    parsedDate = parse(fbDate)

    key = parsedDate.strftime("%Y-%m-%d")

    if parsedDate.strftime("%Y-%m-%d") >= "2015-12-26" and parsedDate.strftime("%Y-%m-%d") <= "2017-03-31":
        if not key in fbList:
            fbList[key] = 1
        else:
            fbList[key] += 1

# output summary to CSV file
with open(saveFileAlphaBay, 'w') as f:
    row = ["date", "sales"]

    row = [s.encode('utf-8') for s in row]

    writer = csv.writer(f, quoting=csv.QUOTE_ALL)
    writer.writerow(row)

    for key in sorted(fbList.iterkeys()):
        row = [key, str(fbList[key])]
        row = [s.encode('utf-8') for s in row]
        writer = csv.writer(f, quoting=csv.QUOTE_ALL)
        writer.writerow(row)

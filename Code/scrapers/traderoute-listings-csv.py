# coding=utf8

# produces a CSV file of all Trade Route listings, by scraping listings data from a completed listings crawl

import csv
import os
from bs4 import BeautifulSoup

saveFile = "/path/to/save/directory/traderoute-listings.csv"
crawlDir = "/path/to/traderoute/save/directory/for/listings/crawl/"

productIDs = []

def parse(file, sf):
    contents = ""

    with open(file, 'r') as myfile:
        contents = myfile.read().replace('\n', '')

    soup = BeautifulSoup(contents, 'html.parser')

    products = soup.find_all("div", {"class": "wLf"})

    for product in products:

        id = ""
        image = ""
        name = ""
        priceQuantity = []
        priceUsd = []
        priceBtc = []
        vendor = ""
        sales = ""
        feedbackPositive = ""

        wLfLeft = product.find("div", {"class": "wLfLeft"})
        lidLink = wLfLeft.find("a")
        id = lidLink.get("href")
        id = id[id.find("lid=") + 4:]

        img = wLfLeft.find("img")
        if img != None:
            image = img.get("src")

        wLfRight = product.find("div", {"class": "wLfRight"})

        wLfName = wLfRight.find("div", {"class": "wLfName"})
        nameLink = wLfName.find("a")
        if nameLink != None:
            name = nameLink.get_text()

        wLfPrice = wLfRight.find("div", {"class": "wLfPrice"})

        wLfSingle = wLfPrice.find("div", {"class": "wLf-single"})
        if wLfSingle != None:
            spans = wLfSingle.find_all("span")

            spanIndex = 0

            for span in spans:
                priceInfo = span.get_text()

                spanIndex += 1

                if spanIndex < len(spans) and spans[spanIndex].get_text() != "":
                    priceInfo = priceInfo[:priceInfo.find(spans[spanIndex].get_text())]

                priceInfo = priceInfo.strip()

                if "USD" in priceInfo:
                    priceUsd.append(priceInfo)
                elif "(" in priceInfo:
                    priceBtc.append(priceInfo)
                elif priceInfo != "":
                    priceQuantity.append(priceInfo)
        else:
            wLfChildren = wLfPrice.find_all("div", {"class": "wLfChild"})

            for wLfChild in wLfChildren:
                spans = wLfChild.find_all("span")

                spanIndex = 0

                for span in spans:
                    priceInfo = span.get_text()

                    spanIndex += 1

                    if spanIndex < len(spans) and spans[spanIndex].get_text() != "":
                        priceInfo = priceInfo[:priceInfo.find(spans[spanIndex].get_text())]

                    priceInfo = priceInfo.strip()

                    if "USD" in priceInfo:
                        priceUsd.append(priceInfo)
                    elif "(" in priceInfo:
                        priceBtc.append(priceInfo)
                    elif priceInfo != "":
                        priceQuantity.append(priceInfo)

        wLfVendor = wLfRight.find("div", {"class": "wLfVendor"})

        vendorLink = wLfVendor.find("a")

        if vendorLink != None:
            vendor = vendorLink.get("href")
            vendor = vendor[vendor.find("user=") + 5:]

        sub = wLfVendor.find("sub")
        if sub != None:
            sales = sub.get_text()

        sup = wLfVendor.find("sup")
        if sup != None:
            feedbackPositive = sup.get_text()


        if id not in productIDs:
            row = [id, image, name]

            for i in range(0, 20):
                if i < len(priceQuantity):
                    row.append(priceQuantity[i])
                else:
                    row.append("")

            for i in range(0, 20):
                if i < len(priceUsd):
                    row.append(priceUsd[i])
                else:
                    row.append("")

            for i in range(0, 20):
                if i < len(priceBtc):
                    row.append(priceBtc[i])
                else:
                    row.append("")

            row.append(vendor)
            row.append(sales)
            row.append(feedbackPositive)

            row = [s.encode('utf-8') for s in row]
            writer = csv.writer(sf, quoting=csv.QUOTE_ALL)
            writer.writerow(row)

            productIDs.append(id)


with open(saveFile, 'w') as f:
    row = ["id", "image", "name"]

    for i in range(1, 21):
        row.append("priceQuantity" + str(i))

    for i in range(1, 21):
        row.append("priceUsd" + str(i))

    for i in range(1, 21):
        row.append("priceBtc" + str(i))

    row.append("vendor")
    row.append("sales")
    row.append("feedbackPositive")

    row = [s.encode('utf-8') for s in row]

    writer = csv.writer(f, quoting=csv.QUOTE_ALL)
    writer.writerow(row)

    for file in os.listdir(crawlDir):
        if file.endswith(".html"):
            parse(os.path.join(crawlDir, file), f)
# produces a CSV file containing a summary of active vendors on AlphaBay
# requires the list of unique vendors and AlphaBay's feedbacks to be produced as CSVs first

import csv
from dateutil.parser import parse

vendorsFile = "/path/to/save/directory/vendors-unique.csv"
feedbacksFile = "/path/to/save/directory/alphabay-feedbacks.csv"
saveFile = "/path/to/save/directory/vendors-active.csv"

fList = {}

feedbacks = []
vendors = []

# load all of AlphaBay's feedbacks
with open(feedbacksFile, 'rb') as f:
    reader = csv.reader(f)
    feedbacks = list(reader)

# remove header from list
if len(feedbacks) > 0:
    feedbacks = feedbacks[1:]

# load list of unique vendors and aliases
with open(vendorsFile, 'rb') as f:
    reader = csv.reader(f)
    vendors = list(reader)

# remove header from list
if len(vendors) > 0:
    vendors = vendors[1:]

# produce a list of active vendors for each date
for feedback in feedbacks:
    pDate = feedback[3]
    vendor = feedback[1] + " A"

    if pDate != "":

        parsedDate = parse(pDate)

        if parsedDate.strftime("%Y-%m-%d") >= "2016-03-25" and parsedDate.strftime("%Y-%m-%d") <= "2017-03-31":

            # determine vendor id (row index from unique vendors file)
            vendorID = "x"

            vendorRowIndex = 0

            for vendorRow in vendors:
                if vendor in vendorRow:
                    vendorID = str(vendorRowIndex)
                    break
                else:
                    vendorRowIndex += 1

            key = parsedDate.strftime("%Y-%m-%d")

            if key not in fList:
                fList[key] = [vendorID]
            elif vendorID not in fList[key]:
                vendorList = fList[key]
                vendorList.append(vendorID)
                fList[key] = vendorList

# save the summary of active vendors to a CSV file
with open(saveFile, 'w') as f:
    row = ["Date", "Active"]

    row = [s.encode('utf-8') for s in row]

    writer = csv.writer(f, quoting=csv.QUOTE_ALL)
    writer.writerow(row)

    for key in sorted(fList.iterkeys()):
        date = key
        vendorList = fList[key]

        noActive = len(vendorList)

        row = [date, str(noActive)]
        row = [s.encode('utf-8') for s in row]
        writer = csv.writer(f, quoting=csv.QUOTE_ALL)
        writer.writerow(row)


# encoding=utf8

# produces a CSV list of AlphaBay vendors and their public keys by querying the Grams hidden service
# requires a product scrape of AlphaBay to have been done first

import socks
import socket
import urllib
import urllib2
from bs4 import BeautifulSoup
import csv
import time

productsCsv = "/path/to/save/directory/alphabay-products.csv"

# IP address of SOCKS proxy
socksIP = ""

# SOCKS proxy port
socksPort = 9050

saveFile = "/path/to/save/directory/vendors-alphabay.csv"

vendors = []
vendorList = []

def create_connection(address, timeout=None, source_address=None):
    sock = socks.socksocket()
    sock.connect(address)
    return sock

class NoRedirection(urllib2.HTTPErrorProcessor):
    def http_response(self, request, response):
        return response

    https_response = http_response

def sendResponse(url, data=None, noRedirect=False):
    request = urllib2.Request(url)

    if (data != None):
        request.add_data(urllib.urlencode(data))

    opener = urllib2

    if (noRedirect == False):
        opener = urllib2.build_opener(urllib2.HTTPHandler(), cookieprocessor)
    else:
        opener = urllib2.build_opener(NoRedirection, cookieprocessor)

    content = ""

    try:
        sock = opener.open(request, timeout=60)
        content = sock.read()
        sock.close()
    except:
        pass

    return content


socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, socksIP, socksPort)

# patch the socket module - to ensure DNS lookups are done over TOR
# https://stackoverflow.com/questions/5148589/python-urllib-over-tor
socket.socket = socks.socksocket
socket.create_connection = create_connection

with open(productsCsv, 'rb') as f:
    reader = csv.reader(f)
    vendorList = list(reader)

# remove header from list
if len(vendorList) > 0:
    vendorList = vendorList[1:]

# obtain a unique list of AlphaBay vendors to query
for row in vendorList:
    vendor = row[5]
    if vendor not in vendors and len(vendor) > 1:
        vendors.append(vendor)

# output the headers for the CSV file
with open(saveFile, 'w') as f:
    row = ["vendor", "pgpKey"]

    row = [s.encode('utf-8') for s in row]

    writer = csv.writer(f, quoting=csv.QUOTE_ALL)
    writer.writerow(row)

# query the public key for each vendor on Grams
for vendor in vendors:
    nonce = None

    # obtain a valid nonce
    while nonce == None:
        cookieprocessor = urllib2.HTTPCookieProcessor()

        content = sendResponse("http://grams7enufi7jmdl.onion/infodesk")

        soup = BeautifulSoup(content, 'html.parser')

        nonce = soup.find("input", {"name": "notoday_nottoday"})

        if nonce == None:
            time.sleep(3)


    data = {}

    data["user_name"] = vendor
    data["notoday_nottoday"] = nonce.get("value")
    data["search_pgp"] = ""
    data["searchbutton"] = "Search"

    content = ""

    # attempt to query Grams
    while content == "":
        content = sendResponse("http://grams7enufi7jmdl.onion/infodesk", data)

        # wait 3 seconds and retry if the page cannot be downloaded
        if content == "":
            time.sleep(3)


    soup = BeautifulSoup(content, 'html.parser')

    frm = soup.find("form", {"name": "info_search_form"})

    vendorLink = ""

    # get the URL to retrieve the public key from, if InfoDesk reports a result for the vendor's name query
    if frm != None:
        links = frm.find_all("a")

        for link in links:
            linkText = link.get_text()

            if "[Alphabay]" in linkText:
                vendorLink = link.get("href")

    content = ""

    if vendorLink != "":
        content = sendResponse(vendorLink)

    soup = BeautifulSoup(content, 'html.parser')

    pgpKey = ""

    # retrieve public key if it exists
    if content != "":
        pre = soup.find("pre")

        if pre != None:
            pgpKey = pre.get_text()


    # output vendor name and public key to CSV file
    with open(saveFile, 'ab') as f:
        row = [vendor, pgpKey]

        writer = csv.writer(f, quoting=csv.QUOTE_ALL)
        writer.writerow(row)

# coding=utf8

# produces a CSV file of all Trade Route vendors, by scraping vendors data from a completed vendors crawl

import csv
import os
from bs4 import BeautifulSoup

saveFile = "/path/to/save/directory/traderoute-vendors.csv"
crawlDir = "/path/to/traderoute/save/directory/for/vendors/crawl/"

def parse(file, sf):
    contents = ""

    with open(file, 'r') as myfile:
        contents = myfile.read().replace('\n', '')

    # trim content
    contents = contents[contents.find("<label>Member since</label>"):]


    soup = BeautifulSoup(contents, 'html.parser')


    name = ""
    joinDate = ""
    pgpKey = ""

    name = file[len(crawlDir):]
    name = name[:len(name) - 5]


    span = soup.find("span")

    if span != None:
        joinDate = span.get_text()

    pre = soup.find("pre")

    if pre != None:
        pgpKey = pre.get_text()



    row = [name, joinDate, pgpKey]
    row = [s.encode('utf-8') for s in row]
    writer = csv.writer(sf, quoting=csv.QUOTE_ALL)
    writer.writerow(row)


with open(saveFile, 'w') as f:
    row = ["name", "joinDate", "pgpKey"]

    row = [s.encode('utf-8') for s in row]

    writer = csv.writer(f, quoting=csv.QUOTE_ALL)
    writer.writerow(row)

    for file in os.listdir(crawlDir):
        if file.endswith(".html"):
            parse(os.path.join(crawlDir, file), f)
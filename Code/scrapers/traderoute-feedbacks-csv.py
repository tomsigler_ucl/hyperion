# coding=utf8

# produces a CSV file of all Trade Route feedbacks, by scraping feedbacks data from a completed products crawl

import csv
import os
from bs4 import BeautifulSoup

saveFile = "/path/to/save/directory/traderoute-feedbacks.csv"
crawlDir = "/path/to/traderoute/save/directory/for/products/crawl/"

def parse(file, sf):
    contents = ""

    with open(file, 'r') as myfile:
        contents = myfile.read().replace('\n', '')

    # trim content
    contents = contents[contents.find("<section id=\"content1\" class=\"tc_section\">"):]


    soup = BeautifulSoup(contents, 'html.parser')


    id = ""
    vendor = ""
    date = ""

    id = file[len(crawlDir):]
    id = id[:id.find("-")]

    divListingRight = soup.find("div", {"class": "listing_right"})

    if divListingRight != None:
        spanName = divListingRight.find("span")

        if spanName != None:
            name = spanName.get_text().strip()

        vendorLink = divListingRight.find("a")

        if vendorLink != None:
            vendor = vendorLink.get_text().strip()

    contents = contents[contents.find("<section id=\"content2\" class=\"tc_section\">"):]

    soup = BeautifulSoup(contents, 'html.parser')

    feedbacks = soup.find_all("div", {"class": "feedback"})

    for feedback in feedbacks:
        date = ""

        span = feedback.find("span")

        date = span.get_text().strip()

        if date.find("2016") != -1:
            date = date[:date.find("2016") + 4]
        elif date.find("2017") != -1:
            date = date[:date.find("2017") + 4]

        row = [id, vendor, date]
        row = [s.encode('utf-8') for s in row]
        writer = csv.writer(sf, quoting=csv.QUOTE_ALL)
        writer.writerow(row)


with open(saveFile, 'w') as f:
    row = ["product id", "vendor", "date"]

    row = [s.encode('utf-8') for s in row]

    writer = csv.writer(f, quoting=csv.QUOTE_ALL)
    writer.writerow(row)

    for file in os.listdir(crawlDir):
        if file.endswith(".html"):
            parse(os.path.join(crawlDir, file), f)
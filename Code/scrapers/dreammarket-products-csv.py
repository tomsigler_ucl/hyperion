# coding=utf8

# produces a CSV file of all Dream Market products, by scraping products data from a completed products crawl

import csv
import os
from bs4 import BeautifulSoup

saveFile = "/path/to/save/directory/dreammarket-products.csv"
crawlDir = "/path/to/dreammarket/save/directory/for/products/crawl/"

def parse(file, sf):
    contents = ""

    with open(file, 'r') as myfile:
        contents = myfile.read().replace('\n', '')

    # trim content
    contents = contents[contents.find("<div class=\"viewProduct\">"):]

    soup = BeautifulSoup(contents, 'html.parser')


    id = ""
    name = ""
    vendor = ""
    price = ""
    shipsTo = ""
    shipsFrom = ""
    escrow = ""
    description = ""

    id = file[len(crawlDir):]
    id = id[:len(id) - 5]

    title = soup.find("div", {"class": "title"})

    if title != None:
        name = title.get_text().strip()

    tabularDetails = soup.find("div", {"class": "tabularDetails"})

    if tabularDetails != None:
        tdDivs = tabularDetails.find_all("div")

        for tdDiv in tdDivs:
            label = tdDiv.find("label")

            if label != None:
                labelText = label.get_text().strip()

                if labelText == "Vendor":
                    vendorLink = tdDiv.find("a")

                    if vendorLink != None:
                        vendor = vendorLink.get_text().strip()

                elif labelText == "Price":
                    labelSpan = tdDiv.find("span")

                    if labelSpan != None:
                        price = labelSpan.get_text().strip()

                elif labelText == "Ships to":
                    labelSpan = tdDiv.find("span")

                    if labelSpan != None:
                        shipsTo = labelSpan.get_text().strip()

                elif labelText == "Ships from":
                    labelSpan = tdDiv.find("span")

                    if labelSpan != None:
                        shipsFrom = labelSpan.get_text().strip()

                elif labelText == "Escrow":
                    labelSpan = tdDiv.find("span")

                    if labelSpan != None:
                        escrow = labelSpan.get_text().strip()

    offerDescription = soup.find("div", {"id": "offerDescription"})

    if offerDescription != None:
        odPre = offerDescription.find("pre")

        if odPre != None:
            description = odPre.get_text().strip()

    if name != "":
        row = [id, name, vendor, price, shipsTo, shipsFrom, escrow, description]
        row = [s.encode('utf-8') for s in row]
        writer = csv.writer(sf, quoting=csv.QUOTE_ALL)
        writer.writerow(row)


with open(saveFile, 'w') as f:
    row = ["id", "name", "vendor", "price", "shipsTo", "shipsFrom", "escrow", "description"]

    row = [s.encode('utf-8') for s in row]

    writer = csv.writer(f, quoting=csv.QUOTE_ALL)
    writer.writerow(row)

    for file in os.listdir(crawlDir):
        if file.endswith(".html"):
            parse(os.path.join(crawlDir, file), f)
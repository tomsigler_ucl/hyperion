# crawls all product pages on the AlphaBay site and saves them to a directory

import socks
import socket
import urllib
import urllib2
from bs4 import BeautifulSoup
from PIL import Image
from StringIO import StringIO
import time
import threading
import os
import pickle
from decimal import Decimal

# IP address of SOCKS proxy
socksIP = ""

# SOCKS proxy port
socksPort = 9050

# directory to save product pages to
saveDir = "/path/to/alphabay/save/directory/for/products/crawl/"

# last product ID to crawl
lastIndex = 365987

# minimum expected size of returned HTML products page
minSize = 100000

# account credentials - each (username, password) tuple will spawn a thread
# add more tuples to generate more threads and decrease time required to crawl
credentials = {("username1", "password1"), ("username2", "password2"), ("username3", "password3")}

quit = False
captchaIndex = 0
downloadErrors = []
errorLock = threading.RLock()
statusLock = threading.RLock()
status = {}

# ensure new outbound connections are created through the SOCKS proxy
def create_connection(address, timeout=None, source_address=None):
    sock = socks.socksocket()
    sock.connect(address)
    return sock

# handler to prevent automatic redirections of page requests
class NoRedirection(urllib2.HTTPErrorProcessor):
    
    def http_response(self, request, response):
        return response
    
    https_response = http_response

# set the value for a field used in the crawling status report
def setStatusValue(field, value):
    global statusLock
    global status
    
    statusLock.acquire()
    try:
        status[field] = value
    finally:
        statusLock.release()

# check if the crawl has finished or has been terminated by the user
def checkCompletion():
    global statusLock
    global credentials
    global status
    
    completed = False
    
    while (completed == False and quit == False):
        completed = True
        
        statusLock.acquire()
        try:
            userIndex = 0
            for credential in credentials:
                userIndex += 1
                
                if userIndex == 1:
                    currentRetry = status["currentRetry" + str(userIndex)]
                    totalRetry = status["totalRetry" + str(userIndex)]
                
                    if currentRetry != totalRetry:
                        completed = False
                
                currentDownload = status["current" + str(userIndex)]
                totalDownloads = status["total" + str(userIndex)]
                
                if currentDownload != totalDownloads:
                    completed = False
    
        finally:
            statusLock.release()

        time.sleep(1)

    statusLock.acquire()
    errorLock.acquire()
    try:
        print ""
        print "Finished with " + str(len(downloadErrors)) + " errors"
    finally:
        statusLock.release()
        errorLock.release()

# crawl a range of product pages with the specified username and password
def crawl(username, password, startIndex, endIndex, userIndex):

    # output error, referencing product with product's ID, to the error log
    def saveError(productID):
        
        global downloadErrors
        global errorLock
        
        errorLock.acquire()
        
        try:
            if (productID not in downloadErrors):
                downloadErrors.append(productID)
                with open(saveDir + "errors.txt", "wb") as fp:
                    pickle.dump(downloadErrors, fp)
        finally:
            errorLock.release()

    # return the page content of a URL, optionally posting parameters and setting requests to not automatically
    # redirect
    def sendResponse(url, data=None, noRedirect=False):
        request = urllib2.Request(url)
    
        if (data != None):
            request.add_data(urllib.urlencode(data))
    
        opener=urllib2
    
        if (noRedirect == False):
            opener=urllib2.build_opener(urllib2.HTTPHandler(), cookieprocessor)
        else:
            opener=urllib2.build_opener(NoRedirection, cookieprocessor)
        
        downloadError = False
        
        content = ""
        statusCode = 0
        
        try:
            sock=opener.open(request, timeout=60)
            content=sock.read()
            statusCode = sock.getcode()
            sock.close()
        except:
            downloadError = True

        # if response status code is not redirect or ok, an error has occurred
        if statusCode != 302 and statusCode != 200:
            downloadError = True

        # log the error if this is a request for a product page and there is an error
        if url.find("listing.php?id=") != -1 and (downloadError or (len(content) < minSize and len(content) != 0)):
            saveError(url[url.find("listing.php?id=") + 15:])
        
        return content

    # download and display the CAPTCHA image referred to by a URL
    def showCaptcha(url):
        content = sendResponse(url)
        img = Image.open(StringIO(content))
        img.show()


    # get a list of files in the crawl directory
    files = os.listdir(saveDir)
    
    setStatusValue("currentRetry" + str(userIndex), "0")
    setStatusValue("totalRetry" + str(userIndex), "0")
    
    setStatusValue("current" + str(userIndex), "0")
    setStatusValue("total" + str(userIndex), str((endIndex - startIndex) + 1))
    
    startIndexOriginal = startIndex

    # determine if a crawl is being resumed and, if so, update the product to start from
    for counter in range(endIndex, startIndex - 1, -1):
        if str(counter) + ".html" in files:
            startIndex = counter + 1
            break

    setStatusValue("current" + str(userIndex), str(startIndex - startIndexOriginal))

    # ensure cookies are kept and associated with the session
    cookieprocessor = urllib2.HTTPCookieProcessor()

    # obtain the login page contents
    content = sendResponse("http://pwoah7foa6au2pul.onion/login.php")

    soup = BeautifulSoup(content, 'html.parser')

    # get the URL for the CAPTCHA image
    captcha_src = soup.find(id="captcha").get("src")

    correctCaptcha = False

    # prompt the user for a solution to the CAPTCHA, submit it with the username and password
    # re-prompt if the login fails
    while correctCaptcha == False:

        showCaptcha("http://pwoah7foa6au2pul.onion/" + captcha_src)

        captcha_code = raw_input("Enter Captcha code " + str(userIndex) + ": ")

        data = {"user": username, "pass": password, "captcha_code": captcha_code}
        content = sendResponse("http://pwoah7foa6au2pul.onion/login.php", data)

        soup = BeautifulSoup(content, 'html.parser')
        
        # check if CAPTCHA entered was correct
        if soup.find(id="captcha") == None:
            correctCaptcha = True
        else:
            captcha_src = soup.find(id="captcha").get("src")
            print "Incorrect Captcha, please try again"
    
    global captchaIndex
    global downloadErrors
    global minSize
    global errorLock
    
    captchaIndex += 1

    # check if there were any errors with downloaded products and re-attempt downloads on first thread only
    if userIndex == 1 and os.path.isfile(saveDir + "errors.txt"):

        # load list of URLs to retry from error log
        errorLock.acquire()
        try:
            with open(saveDir + "errors.txt", "rb") as fp:
                retries = pickle.load(fp)
        finally:
            errorLock.release()

        downloadErrors = retries[:]

        setStatusValue("totalRetry" + str(userIndex), str(len(retries)))

        retryIndex = 0

        # retry each previously erroneous URL and save it on success
        for retry in retries:
            retryIndex += 1
            
            setStatusValue("currentRetry" + str(userIndex), str(retryIndex))
            
            content = sendResponse("http://pwoah7foa6au2pul.onion/listing.php?id=" + str(retry), None, True)

            if len(content) > minSize:
                text_file = open(saveDir + str(retry) + ".html", "w")
                text_file.write(content)
                text_file.close()
            
            if len(content) > minSize or len(content) == 0:
                errorLock.acquire()
                    
                try:
                    downloadErrors.remove(retry)
                    with open(saveDir + "errors.txt", "wb") as fp:
                        pickle.dump(downloadErrors, fp)
                finally:
                    errorLock.release()
                    
            # delay next crawl by 1 second to ensure DDOS defences are not activated
            time.sleep(1)
        
            if quit:
                break

    # crawl each product HTML page and save it to the save directory
    for i in range(startIndex, endIndex + 1):
        setStatusValue("current" + str(userIndex), str(i - startIndexOriginal))
        
        content = sendResponse("http://pwoah7foa6au2pul.onion/listing.php?id=" + str(i), None, True)

        if len(content) != 0:
            text_file = open(saveDir + str(i) + ".html", "w")
            text_file.write(content)
            text_file.close()

        # delay next crawl by 1 second to ensure DDOS defences are not activated
        time.sleep(1)

        if quit:
            break

        # update crawl status
        setStatusValue("current" + str(userIndex), str((endIndex - startIndexOriginal) + 1))

# SOCKS proxy configuration setup
socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, socksIP, socksPort)

# patch the socket module - to ensure DNS lookups are done over TOR
# https://stackoverflow.com/questions/5148589/python-urllib-over-tor
socket.socket = socks.socksocket
socket.create_connection = create_connection


userTotal = len(credentials)
userIndex = 0

# split the crawling load between each set of user credentials and spawn a thread for each set of credentials
for credential in credentials:
    userIndex += 1
    startIndex = (((lastIndex / userTotal) * userIndex) - (lastIndex / userTotal))
    if userIndex != 1:
        startIndex += 1
    endIndex = ((lastIndex / userTotal) * userIndex)
    
    if (userIndex == userTotal):
        endIndex = lastIndex
    
    username, password = credential
    
    crawl_thread = threading.Thread(target=crawl,args = (username, password, startIndex, endIndex, userIndex))

    crawl_thread.start()

    # wait until this thread has spawned, before spawning the next one
    while (captchaIndex != userIndex):
        time.sleep(1)

# start a thread to monitor when the crawl has completed
check_completion_thread = threading.Thread(target=checkCompletion, args = ())
check_completion_thread.start()

# display menu options to user
while (quit == False):
    commandInput = raw_input("Enter (s)tatus or (q)uit: ")

    if commandInput == "q":
        quit = True
    elif commandInput == "s":
        statusLock.acquire()
        try:
            # output the status report
            userIndex = 0
            for credential in credentials:
                userIndex += 1
                
                noThread = str(userIndex)
                currentDownload = status["current" + str(userIndex)]
                totalDownloads = status["total" + str(userIndex)]
                progress = str(round(Decimal((float(currentDownload) / float(totalDownloads)) * 100), 2))
                currentRetry = status["currentRetry" + str(userIndex)]
                totalRetry = status["totalRetry" + str(userIndex)]
                
                if currentRetry != totalRetry:
                    progress = str(round(Decimal(((float(currentDownload) + float(currentRetry)) / (
                    float(totalDownloads) + float(totalRetry))) * 100), 2))
                    print "Thread " + noThread + ": " + progress + "% complete (" + currentRetry + "/" + totalRetry + " retries & " + currentDownload + "/" + totalDownloads + ")"
                else:
                    progress = str(round(Decimal((float(currentDownload) / float(totalDownloads)) * 100), 2))
                    print "Thread " + noThread + ": " + progress + "% complete (" + currentDownload + "/" + totalDownloads + ")"

        finally:
            statusLock.release()

# coding=utf8

# produces a CSV file of all Trade Route products, by scraping products data from a completed products crawl

import csv
import os
from bs4 import BeautifulSoup

saveFile = "/path/to/save/directory/traderoute-products.csv"
crawlDir = "/path/to/traderoute/save/directory/for/products/crawl/"

def parse(file, sf):
    contents = ""

    with open(file, 'r') as myfile:
        contents = myfile.read().replace('\n', '')

    # trim content
    contents = contents[contents.find("<section id=\"content1\" class=\"tc_section\">"):]


    soup = BeautifulSoup(contents, 'html.parser')


    id = ""
    dtImage = ""
    name = ""
    vendor = ""
    price = ""
    description = ""

    id = file[len(crawlDir):]
    id = id[:len(id) - 7]

    divImage = soup.find("div", {"class": "listing_image"})

    if divImage != None:
        img = divImage.find("img")

        if img != None:
            src = img.get("src")

            if src != None and src.find("?") != -1:
                dtImage = src[src.find("?") + 1:]

    divListingRight = soup.find("div", {"class": "listing_right"})

    if divListingRight != None:
        spanName = divListingRight.find("span")

        if spanName != None:
            name = spanName.get_text().strip()

        vendorLink = divListingRight.find("a")

        if vendorLink != None:
            vendor = vendorLink.get_text().strip()

        ps = soup.find_all("p")

        for p in ps:
            if price == "":
                price = p.get_text().strip()
                if " $ (" not in price:
                    price = ""
            elif description == "":
                description = p.get_text().strip()

    if name != "":
        row = [id, dtImage, name, vendor, price]
        row = [s.encode('utf-8') for s in row]
        writer = csv.writer(sf, quoting=csv.QUOTE_ALL)
        writer.writerow(row)


with open(saveFile, 'w') as f:
    row = ["id", "dtImage", "name", "vendor", "price"]

    row = [s.encode('utf-8') for s in row]

    writer = csv.writer(f, quoting=csv.QUOTE_ALL)
    writer.writerow(row)

    for file in os.listdir(crawlDir):
        if file.endswith("-1.html"):
            parse(os.path.join(crawlDir, file), f)
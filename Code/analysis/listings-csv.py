# produces a summary of product listing dates in separate CSV files for AlphaBay, Valhalla and TradeRoute

import csv
from dateutil.parser import parse

# files to save summaries to
saveFiles = ["/path/to/save/directory/availability-alphabay.csv",
             "/path/to/save/directory/availability-valhalla.csv",
             "/path/to/save/directory/availability-traderoute.csv"]

# the product CSV scrapes to obtain product information from
listingFiles = ["/path/to/save/directory/alphabay-products.csv",
                "/path/to/save/directory/valhalla-products.csv",
                "/path/to/save/directory/traderoute-products.csv"]

# the column index that contains the date, for each CSV scrape
dateIndices = [7, 5, 1]

noFiles = len(saveFiles)

for i in range(0, noFiles):
    saveFile = saveFiles[i]
    listingFile = listingFiles[i]
    dateIndex = dateIndices[i]

    pList = {}

    pDNM = []

    # load in a list of all products for the site
    with open(listingFile, 'rb') as f:
        reader = csv.reader(f)
        pDNM = list(reader)

    # remove header from list
    if len(pDNM) > 0:
        pDNM = pDNM[1:]

    for row in pDNM:
        pDate = row[dateIndex]

        if pDate != "":
            parsedDate = parse(pDate)

            key = parsedDate.strftime("%Y-%m-%d")

            # add date and set/increment value by 1 if date is between 25/03/2016 and 31/03/2017
            if parsedDate.strftime("%Y-%m-%d") >= "2016-03-25" and parsedDate.strftime("%Y-%m-%d") <= "2017-03-31":
                if not key in pList:
                    pList[key] = 1
                else:
                    pList[key] += 1


    # output summary to CSV file
    with open(saveFile, 'w') as f:
        row = ["Date", "Listings"]

        row = [s.encode('utf-8') for s in row]

        writer = csv.writer(f, quoting=csv.QUOTE_ALL)
        writer.writerow(row)

        for key in sorted(pList.iterkeys()):
            row = [key, str(pList[key])]
            row = [s.encode('utf-8') for s in row]
            writer = csv.writer(f, quoting=csv.QUOTE_ALL)
            writer.writerow(row)

# produces a CSV summary of average market share %, broken down by day, for AlphaBay
# requires unique vendors, AlphaBay feedback summaries and AlphaBay sales CSVs to be created first

import csv
from dateutil.parser import parse

vendorsFile = "/path/to/save/directory/vendors-unique.csv"
feedbacksFile = "/path/to/save/directory/alphabay-feedbacks.csv"
salesFile = "/path/to/save/directory/sales-alphabay.csv"

# CSV file to output summary to
saveFile = "/path/to/save/directory/market-share-alphabay.csv"

fList = {}

feedbacks = []
vendors = []
feedbackTotals = []
shares = {}

# load feedback summaries
with open(feedbacksFile, 'rb') as f:
    reader = csv.reader(f)
    feedbacks = list(reader)

# remove header from list
if len(feedbacks) > 0:
    feedbacks = feedbacks[1:]

# load vendors
with open(vendorsFile, 'rb') as f:
    reader = csv.reader(f)
    vendors = list(reader)

# remove header from list
if len(vendors) > 0:
    vendors = vendors[1:]

# load sales summaries
with open(salesFile, 'rb') as f:
    reader = csv.reader(f)
    feedbackTotals = list(reader)

# remove header from list
if len(feedbackTotals) > 0:
    feedbackTotals = feedbackTotals[1:]

# build up list of vendors for each date and total of feedbacks for that vendor
for feedback in feedbacks:
    pDate = feedback[3]
    vendor = feedback[1] + " A"

    if pDate != "":

        parsedDate = parse(pDate)

        if parsedDate.strftime("%Y-%m-%d") >= "2016-03-25" and parsedDate.strftime("%Y-%m-%d") <= "2017-03-31":

            # determine vendor id (row index from unique vendors file)
            vendorID = "x"

            vendorRowIndex = 0

            for vendorRow in vendors:
                if vendor in vendorRow:
                    vendorID = str(vendorRowIndex)
                    break
                else:
                    vendorRowIndex += 1

            key = parsedDate.strftime("%Y-%m-%d") + "|" + vendorID

            if key not in fList:
                fList[key] = 1
            else:
                fList[key] += 1

# produce a list of market shares in the format of:
# shares["date"] = [market share 1, market share 2, etc.]

for key in sorted(fList.iterkeys()):
    fKey = key
    fTotalForVendor = fList[key]
    fDate = fKey[:fKey.find("|")]

    fTotalForDay = 0

    for fTotalRow in feedbackTotals:
        if fDate == fTotalRow[0]:
            fTotalForDay = int(fTotalRow[1])

            break

    if fTotalForDay != 0:
        if fDate not in shares:
            shares[fDate] = [float(fTotalForVendor)/float(fTotalForDay)]
        else:
            shareList = shares[fDate]
            shareList.append(float(fTotalForVendor)/float(fTotalForDay))
            shares[fDate] = shareList


# calculate the average market share % for all days and output to a CSV file
with open(saveFile, 'w') as f:
    row = ["date", "share"]

    row = [s.encode('utf-8') for s in row]

    writer = csv.writer(f, quoting=csv.QUOTE_ALL)
    writer.writerow(row)

    for key in sorted(shares.iterkeys()):
        date = key
        shareList = shares[key]

        totalShares = 0
        for share in shareList:
            totalShares += share

        average = (totalShares / len(shareList)) * 100

        row = [date, str(average)]
        row = [s.encode('utf-8') for s in row]
        writer = csv.writer(f, quoting=csv.QUOTE_ALL)
        writer.writerow(row)


# coding=utf8

# produces a CSV file of all Valhalla vendors, by scraping vendors data from a completed vendors crawl

import csv
import os
from bs4 import BeautifulSoup
import codecs

saveFile = "/path/to/save/directory/valhalla-vendors.csv"
crawlDir = "/path/to/valhalla/save/directory/for/vendors/crawl/"


def parse(file, sf):
    contents = ""

    with codecs.open(file, 'r', encoding='utf8') as myfile:
        contents = myfile.read().replace('\n', '')

    url = ""
    alert = ""
    name = ""
    feedbackPositive = ""
    feedbackNegative = ""
    revenue = ""
    lastSignIn = ""
    finalisingEarly = ""
    shipsFrom = ""
    description = ""
    publicKey = ""



    url = "/" + file[len(crawlDir):-5]

    soup = BeautifulSoup(contents, 'html.parser')

    divAlert = soup.find("div", {"class": "alert"})
    if divAlert != None:
        alert = divAlert.get_text()

    h1 = soup.find("h1")
    if h1 != None:
        name = h1.get_text()

    # trim content
    contents = contents[contents.find("</h1>") + 5:]
    soup = BeautifulSoup(contents, 'html.parser')

    for span in soup.find("ul", {"class": "vendor-specs"}).find_all("span"):
        if span.get("class") != None:
            if "green" in span.get("class"):
                feedbackPositive = span.get_text()
            elif "red" in span.get("class"):
                feedbackNegative = span.get_text()

    liIndex = 0

    for li in soup.find_all("li"):
        if liIndex == 2:
            revenue = li.get_text()
            if ":" in revenue:
                revenue = revenue[revenue.find(":") + 2:]
        if liIndex == 3:
            lastSignIn = li.get_text()
            if ":" in lastSignIn:
                lastSignIn = lastSignIn[lastSignIn.find(":") + 2:]
        if liIndex == 4:
            finalisingEarly = li.get_text()
            if ":" in finalisingEarly:
                finalisingEarly = finalisingEarly[finalisingEarly.find(":") + 2:]
        if liIndex == 5:
            shipsFrom = li.get_text()
            if ":" in shipsFrom:
                shipsFrom = shipsFrom[shipsFrom.find(":") + 2:]

        liIndex += 1

    divDescription = soup.find("div", {"class": "description"})
    if divDescription != None:
        description = divDescription.get_text()

    prePgp = soup.find("pre", {"class": "pgp"})
    if prePgp != None:
        publicKey = prePgp.get_text()


    row = [url, name, alert, feedbackPositive, feedbackNegative, revenue, lastSignIn, finalisingEarly, shipsFrom, publicKey]
    row = [s.encode('utf-8') for s in row]
    writer = csv.writer(sf, quoting=csv.QUOTE_ALL)
    writer.writerow(row)


with open(saveFile, 'w') as f:
    row = ["url", "name", "alert", "feedbackPositive", "feedbackNegative", "revenue", "lastSignIn", "finalisingEarly", "shipsFrom", "publicKey"]

    row = [s.encode('utf-8') for s in row]

    writer = csv.writer(f, quoting=csv.QUOTE_ALL)
    writer.writerow(row)

    for file in os.listdir(crawlDir):
        if file.endswith(".html"):
            parse(os.path.join(crawlDir, file), f)
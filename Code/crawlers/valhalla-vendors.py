# crawls all vendor profile pages on the Valhalla site and saves them to a directory

import socks
import socket
import urllib
import urllib2
from bs4 import BeautifulSoup
from PIL import ImageFont
from PIL import Image
from PIL import ImageDraw
from StringIO import StringIO
import time
import threading
import os
import pickle
from decimal import Decimal
import csv

# IP address of SOCKS proxy
socksIP = ""

# SOCKS proxy port
socksPort = 9050

# user account credentials
credentials = ("username", "password")

# full path and filename to true type font to render text with on images
fontFile = "Arial.ttf"

# minimum expected size of returned HTML products page
minSize = 2000

# directory to save product pages to
saveDir = "/path/to/valhalla/save/directory/for/vendors/crawl/"

# CSV file containing scraped products from previous products crawl
productsCsv = "/path/to/save/directory/valhalla-products.csv"

# number of crawling threads to spawn
userTotal = 1

quit = False
captchaIndex = 0
downloadErrors = []
errorLock = threading.RLock()
statusLock = threading.RLock()
status = {}
vendors = []

# handler to prevent automatic redirections of page requests
class NoRedirection(urllib2.HTTPErrorProcessor):
    def http_response(self, request, response):
        return response

    https_response = http_response

# ensure new outbound connections are created through the SOCKS proxy
def create_connection(address, timeout=None, source_address=None):
    sock = socks.socksocket()
    sock.connect(address)
    return sock

# set the value for a field used in the crawling status report
def setStatusValue(field, value):
    global statusLock
    global status

    statusLock.acquire()
    try:
        status[field] = value
    finally:
        statusLock.release()

# check if the crawl has finished or has been terminated by the user
def checkCompletion():
    global statusLock
    global status

    completed = False

    while (completed == False and quit == False):
        completed = True

        statusLock.acquire()
        try:
            userIndex = 0
            for i in range(0, userTotal):
                userIndex += 1

                if userIndex == 1:
                    currentRetry = status["currentRetry" + str(userIndex)]
                    totalRetry = status["totalRetry" + str(userIndex)]

                    if currentRetry != totalRetry:
                        completed = False

                currentDownload = status["current" + str(userIndex)]
                totalDownloads = status["total" + str(userIndex)]

                if currentDownload != totalDownloads:
                    completed = False

        finally:
            statusLock.release()

        time.sleep(1)

    statusLock.acquire()
    errorLock.acquire()
    try:
        print ""
        print "Finished with " + str(len(downloadErrors)) + " errors"
    finally:
        statusLock.release()
        errorLock.release()

# crawl a range of vendor profile pages with the specified username and password
def crawl(username, password, startIndex, endIndex, userIndex):

    # output error, referencing product with product's ID, to the error log
    def saveError(productID):

        global downloadErrors
        global errorLock

        errorLock.acquire()

        try:
            if (productID not in downloadErrors):
                downloadErrors.append(productID)
                with open(saveDir + "errors.txt", "wb") as fp:
                    pickle.dump(downloadErrors, fp)
        finally:
            errorLock.release()

    # return the page content of a URL, optionally posting parameters and setting requests to not automatically
    # redirect
    def sendResponse(url, data=None, noRedirect=False):
        request = urllib2.Request(url)

        if (data != None):
            request.add_data(urllib.urlencode(data))

        opener = urllib2

        if (noRedirect == False):
            opener = urllib2.build_opener(urllib2.HTTPHandler(), cookieprocessor)
        else:
            opener = urllib2.build_opener(NoRedirection, cookieprocessor)

        downloadError = False

        content = ""
        statusCode = 0

        try:
            sock = opener.open(request, timeout=60)
            content = sock.read()
            statusCode = sock.getcode()
            sock.close()
        except:
            downloadError = True

        # if response status code is not redirect, ok or not found, an error has occurred
        if statusCode != 302 and statusCode != 200 and statusCode != 404:
            downloadError = True

        if statusCode == 404:
            content = ""

        if "Your request couldn't be processed right now! Please try again in a few minutes and send a message to support if the problem persists." in content:
            downloadError = True
            content = ""

        # log the error if this is a request for a vendor profile page and there is an error
        if url in vendors and (downloadError or (len(content) < minSize and len(content) != 0)):
            saveError(url)

        return content


    # get a list of files in the crawl directory
    files = os.listdir(saveDir)

    setStatusValue("currentRetry" + str(userIndex), "0")
    setStatusValue("totalRetry" + str(userIndex), "0")

    setStatusValue("current" + str(userIndex), "0")
    setStatusValue("total" + str(userIndex), str((endIndex - startIndex) + 1))

    startIndexOriginal = startIndex

    # determine if a crawl is being resumed and, if so, update the vendor profile to start from
    for counter in range(endIndex - 1, startIndex, -1):
        if vendors[counter][1:] + ".html" in files:
            startIndex = counter + 1
            break

    setStatusValue("current" + str(userIndex), str(startIndex - startIndexOriginal))

    # ensure cookies are kept and associated with the session
    cookieprocessor = urllib2.HTTPCookieProcessor()

    # obtain the login page contents
    content = sendResponse("http://valhallaxmn3fydu.onion/login")

    soup = BeautifulSoup(content, 'html.parser')

    correctCaptcha = False

    # prompt the user for a solution to the CAPTCHA, submit it with the username,  password and nonce
    # re-prompt if the login fails
    while correctCaptcha == False:

        captchaImages = []

        challenge = ""
        pChallenge = soup.find("p", {"class": "challenge"})
        if pChallenge != None:
            challenge = pChallenge.get_text()
            if challenge[len(challenge) - 1:] == ".":
                challenge = challenge[:len(challenge) - 1]

        # get the login nonce
        authenticity_token = ""
        fAuthenticity_token = soup.find("input", {"name": "authenticity_token"})
        if fAuthenticity_token != None:
            authenticity_token = fAuthenticity_token.get("value")

        for img in soup.find_all("img", {"class": "img-rounded"}):
            src = img.get("src")

            captchaUrl = "http://valhallaxmn3fydu.onion" + src

            captchaContent = sendResponse(captchaUrl)
            captchaImages.append(StringIO(captchaContent))


        # combine all images into a grid
        images = map(Image.open, captchaImages)
        widths, heights = zip(*(i.size for i in images))

        total_width = sum(widths)
        max_height = max(heights)

        new_img = Image.new('RGB', (total_width, max_height))

        x_offset = 0
        imgIndex = 0

        # label each image in the grid with a number starting at 0
        for img in images:
            draw = ImageDraw.Draw(img)
            draw.rectangle(((0, 00), (36, 36)), fill="white")
            fnt = ImageFont.truetype(fontFile, 16)
            draw.text((10, 10), str(imgIndex), (0, 0, 0), font=fnt)

            new_img.paste(img, (x_offset,0))
            x_offset += img.size[0]

            imgIndex += 1

        new_img.show()

        captcha_code = raw_input(challenge + " (enter all numbers eg. 01267): ")

        data = {"user[name]": username, "user[password]": password, "authenticity_token": authenticity_token}

        for i in range(0, 8):
            captchaValue = "0"
            if str(i) in captcha_code:
                captchaValue = "1"
            data["user[captcha_" + str(i) + "]"] = captchaValue

        content = sendResponse("http://valhallaxmn3fydu.onion/sessions", data)

        soup = BeautifulSoup(content, 'html.parser')

        # check if CAPTCHA entered was correct
        pChallenge = soup.find("p", {"class": "challenge"})
        if pChallenge == None:
            correctCaptcha = True
        else:
            print "Incorrect Captcha, please try again"


    global captchaIndex
    global downloadErrors
    global minSize
    global errorLock

    captchaIndex += 1

    # check if there were any errors with downloaded vendor profiles and re-attempt downloads on first thread only
    if userIndex == 1 and os.path.isfile(saveDir + "errors.txt"):

        # load list of URLs to retry from error log
        errorLock.acquire()
        try:
            with open(saveDir + "errors.txt", "rb") as fp:
                retries = pickle.load(fp)
        finally:
            errorLock.release()

        downloadErrors = retries[:]

        setStatusValue("totalRetry" + str(userIndex), str(len(retries)))

        retryIndex = 0

        # retry each previously erroneous URL and save it on success
        for retry in retries:
            retryIndex += 1

            setStatusValue("currentRetry" + str(userIndex), str(retryIndex))

            content = sendResponse("http://valhallaxmn3fydu.onion" + str(retry), None, True)

            if len(content) > minSize:
                text_file = open(saveDir + str(retry[1:]) + ".html", "w")
                text_file.write(content)
                text_file.close()

            if len(content) > minSize or len(content) == 0:
                errorLock.acquire()

                try:
                    downloadErrors.remove(retry)
                    with open(saveDir + "errors.txt", "wb") as fp:
                        pickle.dump(downloadErrors, fp)
                finally:
                    errorLock.release()

            # delay next crawl by 1 second to ensure DDOS defences are not activated
            time.sleep(1)

            if quit:
                break

    # crawl each vendor profile HTML page and save it to the save directory
    for i in range(startIndex, endIndex + 1):
        setStatusValue("current" + str(userIndex), str(i - startIndexOriginal))

        content = sendResponse("http://valhallaxmn3fydu.onion" + vendors[i], None, True)

        if len(content) != 0:
            text_file = open(saveDir + vendors[i][1:] + ".html", "w")
            text_file.write(content)
            text_file.close()

        # delay next crawl by 1 second to ensure DDOS defences are not activated
        time.sleep(1)

        if quit:
            break

    # update crawl status
    setStatusValue("current" + str(userIndex), str((endIndex - startIndexOriginal) + 1))

# SOCKS proxy configuration setup
socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, socksIP, socksPort)

# patch the socket module - to ensure DNS lookups are done over TOR
# https://stackoverflow.com/questions/5148589/python-urllib-over-tor
socket.socket = socks.socksocket
socket.create_connection = create_connection

userIndex = 0

lastIndex = 0

productList = []

# load the list of previously scraped products
with open(productsCsv, 'rb') as f:
    reader = csv.reader(f)
    productList = list(reader)

# remove header from list
if len(productList) > 0:
    productList = productList[1:]

# ensure no duplicate vendors are added to the list
for row in productList:
    vendor = row[10]
    if vendor not in vendors and len(vendor) > 1:
        vendors.append(row[10])

lastIndex = len(vendors) - 1

# split the crawling load between the specified number of threads
for i in range(0, userTotal):
    userIndex += 1
    startIndex = (((lastIndex / userTotal) * userIndex) - (lastIndex / userTotal))
    if userIndex != 1:
        startIndex += 1
    endIndex = ((lastIndex / userTotal) * userIndex)
    if (userIndex == userTotal):
        endIndex = lastIndex

    username, password = credentials

    crawl_thread = threading.Thread(target=crawl, args=(username, password, startIndex, endIndex, userIndex))

    crawl_thread.start()

    # wait until this thread has spawned, before spawning the next one
    while (captchaIndex != userIndex):
        time.sleep(1)

check_completion_thread = threading.Thread(target=checkCompletion, args=())
check_completion_thread.start()

# display menu options to user
while (quit == False):
    commandInput = raw_input("Enter (s)tatus or (q)uit: ")

    if commandInput == "q":
        quit = True
    elif commandInput == "s":
        statusLock.acquire()
        try:
            # output the status report
            userIndex = 0
            for i in range(0, userTotal):
                userIndex += 1

                noThread = str(userIndex)
                currentDownload = status["current" + str(userIndex)]
                totalDownloads = status["total" + str(userIndex)]
                currentRetry = status["currentRetry" + str(userIndex)]
                totalRetry = status["totalRetry" + str(userIndex)]

                if currentRetry != totalRetry:
                    progress = str(round(Decimal(((float(currentDownload) + float(currentRetry)) / (float(totalDownloads) + float(totalRetry))) * 100), 2))
                    print "Thread " + noThread + ": " + progress + "% complete (" + currentRetry + "/" + totalRetry + " retries & " + currentDownload + "/" + totalDownloads + ")"
                else:
                    progress = str(round(Decimal((float(currentDownload) / float(totalDownloads)) * 100), 2))
                    print "Thread " + noThread + ": " + progress + "% complete (" + currentDownload + "/" + totalDownloads + ")"

        finally:
            statusLock.release()

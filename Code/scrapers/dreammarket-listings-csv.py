# coding=utf8

# produces a CSV file of all Dream Market listings, by scraping listings data from a completed listings crawl

import csv
import os
from bs4 import BeautifulSoup

saveFile = "/path/to/save/directory/dreammarket-listings.csv"
crawlDir = "/path/to/dreammarket/save/directory/for/listings/crawl/"

productIDs = []

def parse(file, sf):
    contents = ""

    with open(file, 'r') as myfile:
        contents = myfile.read().replace('\n', '')

    soup = BeautifulSoup(contents, 'html.parser')

    products = soup.find_all("div", {"class": "around"})

    for product in products:

        id = ""
        name = ""
        escrow = ""
        price = ""
        vendor = ""
        successful = ""
        avgRating = ""
        ships = ""

        oTitle = product.find("div", {"class": "oTitle"})

        oTitleA = oTitle.find("a")
        name = oTitleA.get_text().strip()

        id = oTitleA.get("href")
        id = id[id.find("=") + 1:]

        escrowInfo = product.find("div", {"class": "escrowBox"})
        if escrowInfo != None:
            escrow = escrowInfo.get_text()

        oPrice = product.find("div", {"class": "oPrice"})
        if oPrice != None:
            price = oPrice.get_text().strip()

        oVendor = product.find("div", {"class": "oVendor"})
        if oVendor != None:
            oVendorA = oVendor.find("a")
            vendor = oVendorA.get_text().strip()

            trans = oVendor.find("span", {"title": "Successful transactions"})
            if trans != None:
                successful = trans.get_text()
                successful = successful[1:-1]

            rating = oVendor.find("span", {"class": "userRating"})
            if rating != None:
                avgRating = rating.get_text().strip()

        osBod = product.find("span", {"class": "osBod"})
        if osBod != None:
            ships = osBod.get_text()

        if id not in productIDs:
            row = [id, name, escrow, price, vendor, successful, avgRating, ships]
            row = [s.encode('utf-8') for s in row]
            writer = csv.writer(sf, quoting=csv.QUOTE_ALL)
            writer.writerow(row)

            productIDs.append(id)


with open(saveFile, 'w') as f:
    row = ["id", "name", "escrow", "price", "vendor", "successful", "avgRating", "ships"]

    row = [s.encode('utf-8') for s in row]

    writer = csv.writer(f, quoting=csv.QUOTE_ALL)
    writer.writerow(row)

    for file in os.listdir(crawlDir):
        if file.endswith(".html"):
            parse(os.path.join(crawlDir, file), f)